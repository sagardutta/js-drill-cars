// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. 
// Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. 
// Then log the car's year, make, and model in the console log in the format of:
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

function findCarById(inventory, id) {

    if (inventory == undefined || !Array.isArray(inventory) || inventory.length == 0) {
        return []
    }

    if (id == undefined || isNaN(id)) {
        return []
    }

    for (car in inventory) {
        if (id == inventory[car].id) {
            return {
                id: id,
                car_year: inventory[car].car_year,
                car_make: inventory[car].car_make,
                car_model: inventory[car].car_model
            }
        }
    }
    return []
}

module.exports = findCarById;