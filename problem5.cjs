// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. 
// Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 
// and return the array of older cars and log its length.

const findYearsArray = require('./problem4.cjs')

function findYearsOlderThan2000(inventory) {
    const years = findYearsArray(inventory)
    const olderThan2000 = []

    for (year in years) {
        if (years[year] < 2000) {
            olderThan2000.push(years[year])
        }
    }

    console.log(olderThan2000.length)
    return olderThan2000;
}

module.exports = findYearsOlderThan2000;