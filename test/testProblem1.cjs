const inventory = require('../data/inventory.js')
const findCarById = require('../problem1.cjs')

const obj = findCarById(inventory, 33)
if (obj.id != undefined) {
    console.log(`Car ${obj.id} is a ${obj.car_year} ${obj.car_make} ${obj.car_model}`)
} else {
    console.log(`Car with id not found.`)
}