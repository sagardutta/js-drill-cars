// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. 
// Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function findYearsArray(inventory) {
    const years = []

    for (car in inventory) {
        years.push(inventory[car].car_year)
    }

    return years
}

module.exports = findYearsArray;